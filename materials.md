## Learning materials
- [PostgreSQL Tutorial](https://www.postgresqltutorial.com/)
- [Учебные курсы PostgresPro](https://postgrespro.ru/education/courses)

### Lectures
- [Индексы и производительность](https://www.youtube.com/watch?v=4Tgvd6NPufs)

### Courses
- [Язык SQL](https://youtube.com/playlist?list=PLaFqU3KCWw6J1NEI8hjYlvGnD4Y7Sxx4r)
- [Разработка серверной части приложений PostgreSQL. Базовый курс](https://youtube.com/playlist?list=PLaFqU3KCWw6LNR1IZ814whJe89J1tRQ3t)
- [Разработка серверной части приложений PostgreSQL. Расширенный курс](https://youtube.com/playlist?list=PLaFqU3KCWw6K3AyBVcZGdXMtM2xvjHA1N)
- [Оптимизация запросов](https://youtube.com/playlist?list=PLaFqU3KCWw6K2sTAksX5AJq4SQDN5PA1t)

### Books
- [PostgreSQL 14 Изнутри](https://gitlab.com/procoders.school/databases-hub/-/blob/main/postgresql_internals-14.pdf)


